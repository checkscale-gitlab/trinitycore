#!/usr/bin/env sh

# MIT License
# Copyright (c) 2017-2022 Nicola Worthington <nicolaw@tfb.net>
# https://github.com/neechbear/trinitycore

set -e

isDockerEnv() {
  [ -e /.dockerenv ]
}

isTCContainer() {
  isDockerEnv \
  && [ -e "${TC_CHROOT}/.git-rev" ] \
  && [ -e "${TC_CHROOT}/.git-rev-short" ] \
  && [ -e "${TC_CHROOT}/.tdb-full-url" ]
}

getImageTCLabel() {
  local image="$1"; shift
  local key=""
  for key in "$@" ; do
    local label="org.opencontainers.image.trinitycore.$key"
    local filter="{{ index .Config.Labels \"$label\" }}"
    docker inspect -f "$filter" "$image"
  done
}

printHelp() {
  if isTCContainer ; then
    echo "Usage: gettdb [-h|--help] [LATEST|release-tag]"
  else
    echo "Usage: gettdb [-h|--help] <LATEST|release-tag|image>"
  fi

  echo "Downloads TDB world server database archive from the TrinityCore Git repository."
  echo ""
  echo "Download the most recent version available:"
  echo ""
  echo "  $ gettdb LATEST"
  echo ""
  echo "Download an explcit version by GitHub release tag:"
  echo ""
  echo "  $ gettdb TDB335.22011"
  echo ""

  if isTCContainer ; then
    echo "Download the version required by this container image:"
    echo ""
    echo "  $ gettdb"
    echo ""
  else
    echo "Download the labelled and required by a specific container image:"
    echo ""
    echo "  $ gettdb nicolaw/trinitycore:3.3.5-slim"
    echo ""
  fi
}

getLatestTdbGitHubReleaseUrl() {
  local tag="$(echo "$1" | tr -cd '[TDB\.0-9]')"
  if [ -s ".tdb-full-url.$tag" ] ; then
    cat ".tdb-full-url.$tag"
  else
    curl -sSL https://api.github.com/repos/TrinityCore/TrinityCore/releases \
      | jq -r --arg tag "$tag" '[.[]|select(.tag_name|contains($tag))|select(.assets[0].browser_download_url|endswith(".7z")).assets[].browser_download_url]|max' \
      | tee ".tdb-full-url.$tag"
  fi
}

downloadAndExtractTdb() {
  local url="$1"
  local file="$(echo "$url" | sed -r -e 's/^.*\///')"

  if ! [ -e "$file" ] ; then
    >&2 printf 'Retrieving %s from %s ...\n\n' "$file" "$url"
    curl -LO "$url"
  fi

  if which 7zr >/dev/null && [ -e "$file" ]; then
    #7zr x -y -osrc/sql -- "$file"
    7zr x -y -- "$file"
  fi
}

main() {
  local version=""
  local dryrun=0
  local arg=""

  for arg in "$@" ; do
    if [ "$arg" = "-h" ] || [ "$arg" = "--help" ] ; then
      printHelp >&2
      exit 0
    elif [ "$arg" = "-n" ] || [ "$arg" = "--dry-run" ] ; then
      dryrun=1
    elif [ -z "$version" ] ; then
      version="$arg"
    fi
  done

  if ! isTCContainer && [ -z "$version" ] ; then
    printHelp >&2
    exit 1
  fi

  local url=""
  if isTCContainer && [ -z "$version" ] && [ -s "${TC_CHROOT}/.tdb-full-url" ] ; then
    url="$(cat "${TC_CHROOT}/.tdb-full-url")"

  else
    case "$version" in
      LATEST|latest|3.3.5|335|3.3.5a|335a)
        url="$(getLatestTdbGitHubReleaseUrl "TDB335")"
        ;;
      master)
        url="$(getLatestTdbGitHubReleaseUrl "TDB9")"
        ;;
      TDB*)
        url="$(getLatestTdbGitHubReleaseUrl "$version")"
        ;;
      *)
        url="$(getImageTCLabel "$version" tdb-full-url)"
        ;;
    esac
  fi

  if [ -z "$url" ] ; then
    echo "Failed to discover TDB archive URL; aborting!" >&2
    exit 2
  fi

  if [ "$dryrun" = "1" ] ; then
    echo "$url"
  else
    downloadAndExtractTdb "$url"
  fi
}

main "$@"
